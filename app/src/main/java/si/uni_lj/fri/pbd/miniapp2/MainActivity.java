package si.uni_lj.fri.pbd.miniapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    MediaPlayerService mediaPlayerService;
    boolean serviceBound;

    TextView songTitleText, songDurationText;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound");

            MediaPlayerService.RunServiceBinder binder = (MediaPlayerService.RunServiceBinder) iBinder;
            mediaPlayerService = binder.getService();
            mediaPlayerService.background();
            serviceBound = true;

            // Update the UI if the service is already running the timer
            if(mediaPlayerService.isSongPlaying()) {
                updateUIPlaySong();
                updateUISongDuration();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Service disconnect");

            serviceBound = false;
        }
    };

    /**
     * Receives broadcast from MediaPlayerService as a reminder to update UI elements
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == MediaPlayerService.UPDATE) {

                updateUISongDuration();
                if (mediaPlayerService.isSongPlaying()) {
                    updateUIPlaySong();
                }
                else if (mediaPlayerService.isSongStopped())
                    updateUIStopSong();

            } else if (intent.getAction() == MediaPlayerService.EXIT) {
                exitButtonClick(null);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songTitleText = (TextView) findViewById(R.id.trackTitleText);
        songDurationText = (TextView) findViewById(R.id.trackDurationText);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Starting and binding media player service");

        Intent i = new Intent(this, MediaPlayerService.class);
        startService(i);
        bindService(i, mConnection, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // for receiving broadcasts from MediaPlayService
        // one if for updating the UI every second and the second is for exiting the app
        IntentFilter recvr = new IntentFilter();
        recvr.addAction(MediaPlayerService.UPDATE);
        recvr.addAction(MediaPlayerService.EXIT);

        registerReceiver(mReceiver, recvr);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (serviceBound) {
            unbindService(mConnection);
            serviceBound = false;
        }
    }

    /**
     * Called when user presses the play button
     */
    public void playButtonClick(View v) {
        if(serviceBound) {
            if(!mediaPlayerService.isSongPlaying()) {
                Log.d(TAG, "Starting music");
                mediaPlayerService.playMedia();
                updateUIPlaySong();
            } else {
                Toast.makeText(this, R.string.alreadyPlaying, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Called when user presses the pause button
     */
    public void pauseButtonClick(View v) {
        if(serviceBound) {
            if(mediaPlayerService.isSongPlaying()) {
                Log.d(TAG, "Pausing music");
                mediaPlayerService.pauseMedia();
            } else {
                Toast.makeText(this, R.string.alreadyPaused, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Called when user presses the stop button
     */
    public void stopButtonClick(View v) {
        if(serviceBound) {
            if(!mediaPlayerService.isSongStopped()) {

                Log.d(TAG, "Stopping music");
                mediaPlayerService.stopMedia();
                updateUIStopSong();
            } else {
                Toast.makeText(this, R.string.alreadyStopped, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Called when user presses the exit button
     */
    public void exitButtonClick(View v) {

        // triggers service shutdown
        if(serviceBound) {
            mediaPlayerService.exitMedia();
            unbindService(mConnection);
            serviceBound = false;
        } else {
            Log.e(TAG, "Service not bound.");
        }

        // exit the application
        finish();
    }

    /**
     * Called when user presses the g-on button
     */
    public void gOnButtonClick(View v) {
        if(serviceBound) {
            if(!mediaPlayerService.areGesturesEnabled()) {
                mediaPlayerService.enableGestures();
                Toast.makeText(this, R.string.gesturesOn, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.gesturesAlreadyOn, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Called when user presses the g-off button
     */
    public void gOffButtonClick(View v) {
        if(serviceBound) {
            if(mediaPlayerService.areGesturesEnabled()) {
                mediaPlayerService.disableGestures();
                Toast.makeText(this, R.string.gesturesOff, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.gesturesAlreadyOff, Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Updates the UI when a run starts
     */
    private void updateUIPlaySong() {
        songTitleText.setText(mediaPlayerService.getCurrentSong());
    }

    /**
     * Updates the UI when a run stops
     */
    private void updateUIStopSong() {
        songTitleText.setText(R.string.defaultTitle);
        songDurationText.setText(R.string.defaultDuration);
    }

    /**
     * Updates the song duration readout in the UI; the service must be bound
     */
    private void updateUISongDuration() {

        if(serviceBound) {
            String currentDur = createTimeLabel(mediaPlayerService.getCurrentPos());
            String totalDur = createTimeLabel(mediaPlayerService.getDuration());
            songDurationText.setText(currentDur+" / "+totalDur);
        } else {
            Log.e(TAG, "Service not bound.");
        }
    }

    /**
     * Formats the song duration integer into a readable format
     */
    public String createTimeLabel(int time) {
        String timeLabel = "";
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        timeLabel = min + ":";
        if(sec < 10) timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }
}
