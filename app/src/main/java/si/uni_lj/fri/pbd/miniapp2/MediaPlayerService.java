package si.uni_lj.fri.pbd.miniapp2;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MediaPlayerService extends Service {

    private static final String TAG = MediaPlayerService.class.getSimpleName();
    public static final String ACTION_STOP = "stop_service";
    public static final String ACTION_START = "start_service";
    public static final String ACTION_PAUSE = "pause_service";
    public static final String ACTION_EXIT = "exit_service";
    private static final String channelID = "background_timer";
    public static final String UPDATE = "si.uni_lj.fri.pbd.miniapp2.UPDATE";
    public static final String EXIT = "si.uni_lj.fri.pbd.miniapp2.EXIT";
    private static final int IDLE = 0;
    private static final int VERTICAL = 1;
    private static final int HORIZONTAL = 2;

    public NotificationCompat.Builder builder;

    // Handler to update the UI every second when the song is playing
    private final Handler mUpdateTimeHandler = new UIUpdateHandler();
    // Handler to check the sensor command value evey half second
    private final Handler mCheckSensorHandler = new CheckSensorHandler();

    public static final int NOTIFICATION_ID = 69;
    // Message type for the handler
    private final static int MSG_UPDATE_TIME = 0;
    private final static int MSG_CHECK_SENSOR = 1;

    private MediaPlayer mediaPlayer;
    private boolean isMusicPlaying;
    private boolean isMusicRestart;
    private boolean areGesturesEnabled;

    private String[] files;
    private List<String> songs = new LinkedList<String>();
    private String currentSong = "";

    private final IBinder serviceBinder = new RunServiceBinder();
    AccelerationService accelerationService;
    boolean accelServiceBound;

    private ServiceConnection mAccelConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "AccelerationService bound in onServiceConnected()");

            AccelerationService.RunServiceBinder binder = (AccelerationService.RunServiceBinder) iBinder;
            accelerationService = binder.getService();
            accelServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "AccelerationService disconnect in onServiceDisconnected()");

            accelServiceBound = false;
        }
    };

    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");

        mediaPlayer = new MediaPlayer();

        isMusicPlaying = false;
        isMusicRestart = true;
        accelServiceBound = false;
        areGesturesEnabled = false;

        // save song titles from mp3 files in assets folder
        getSongs();

        // create and display notification
        createNotificationChannel();
        foreground();

        // start the handler to run every second
        mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);

        // start and bind to acceleration service
        Log.d(TAG, "Starting and binding acceleration service");
        Intent i = new Intent(this, AccelerationService.class);
        startService(i);
        bindService(i, mAccelConnection, 0);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service");

        // accept actions when notification buttons pressed
        if(intent.getAction() == ACTION_START) {
            playMedia();
        }
        else if(intent.getAction() == ACTION_STOP) {
            stopMedia();
        }
        else if(intent.getAction() == ACTION_PAUSE) {
            pauseMedia();
        }
        else if(intent.getAction() == ACTION_EXIT) {
            exitMedia();
        }

        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return serviceBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Destroying service");
        background();

    }

    /**
     * Starts the media player
     */
    public void playMedia() {
        if(isMusicRestart) {
            if(!isMusicPlaying) {
                Log.d(TAG, "playing a fresh new song");
                String title = getRandomSong();
                try {
                    AssetFileDescriptor afd = this.getAssets().openFd(title);
                    mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    afd.close();
                    mediaPlayer.prepare();
                } catch (final Exception e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
                isMusicPlaying = true;
                isMusicRestart = false;
            } else {
                Log.e(TAG, "playMedia request for an already running media player");
            }
        } else {
            if(!isMusicPlaying) {
                mediaPlayer.start();
                isMusicPlaying = true;
            }

        }
    }

    /**
     * Pauses the media player
     */
    public void pauseMedia() {
        if(isMusicPlaying) {
            mediaPlayer.pause();
            isMusicPlaying = false;
        } else {
            Log.e(TAG, "pauseMedia request for an already paused media player");
        }
    }

    /**
     * Stops the media player
     */
    public void stopMedia() {
        if(!isMusicRestart) {
            mediaPlayer.reset();
            //mediaPlayer.release();
            isMusicPlaying = false;
            isMusicRestart = true;
        } else {
            Log.e(TAG, "stopMedia request for an already stopped media player");
        }
    }

    /**
     * Exits the media player
     */
    public void exitMedia() {
        // stops music if playing
        if(isMusicPlaying) {
            stopMedia();
        }

        // stops AccelerationService
        Log.d(TAG, "Stopping and unbinding acceleration service");
        mCheckSensorHandler.removeMessages(MSG_CHECK_SENSOR);
        unbindService(mAccelConnection);
        accelServiceBound = false;
        Intent i = new Intent(this, AccelerationService.class);
        stopService(i);

        // stops MediaPlayerService and all of its components if running
        mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);

        background();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);

        stopSelf();

        // tells main activity to exit
        Intent intent = new Intent(EXIT);
        MediaPlayerService.this.sendBroadcast(intent);
    }

    /**
     * Enables the gestures
     */
    public void enableGestures() {
        if(accelServiceBound) {
            if(!areGesturesEnabled) {
                Log.d(TAG, "Enabling gestures");
                mCheckSensorHandler.sendEmptyMessage(MSG_CHECK_SENSOR);
                areGesturesEnabled = true;
            }
        } else {
            Log.d(TAG, "Acceleration service not bound");
        }
    }

    /**
     * Disables the gestures
     */
    public void disableGestures() {
        if (accelServiceBound) {
            if(areGesturesEnabled) {
                Log.d(TAG, "Disabling gestures");
                mCheckSensorHandler.removeMessages(MSG_CHECK_SENSOR);
                areGesturesEnabled = false;
            }
        } else {
            Log.d(TAG, "Acceleration service not bound");
        }
    }

    /**
     * Returns true if gestures are enabled
     */
    public boolean areGesturesEnabled() {
        return areGesturesEnabled;
    }

    /**
     * Returns true if song is playing
     */
    public boolean isSongPlaying() {
        return isMusicPlaying;
    }

    /**
     * Returns true if song is stopped
     */
    public boolean isSongStopped() {
        return isMusicRestart;
    }

    /**
     * Returns duration of song
     */
    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    /**
     * Returns returns current duration of song
     */
    public int getCurrentPos() {
        return mediaPlayer.getCurrentPosition();
    }

    /**
     * Returns the current song if any are playing
     */
    public String getCurrentSong() {
        if(!isMusicRestart) {
            return currentSong;
        } else {
            return getString(R.string.defaultTitle);
        }
    }

    /**
     * Saves mp3 file names from assets folder
     */
    public void getSongs() {
        AssetManager assetManager = getAssets();
        try {
            files = new String[0];
            files = assetManager.list("");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i=0; i<files.length; i++) {
            //Log.d(TAG, files[i] + " of " + files.length + " files.");
            if(files[i].contains(".mp3")) {
                songs.add(files[i]);
            }
        }
    }

    /**
     * Returns a random song title from the ones found in the assets folder
     */
    public String getRandomSong() {
        int max = songs.size()-1;
        int min = 0;
        Random r = new Random();
        int songIx = r.nextInt((max - min) + 1) + min;
        currentSong = songs.get(songIx);
        return currentSong;
    }

    /**
     * When the timer is running, use this handler to update
     * the UI every second to show timer progress
     */
    class UIUpdateHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;

        @Override
        public void handleMessage(Message message) {
            if (MSG_UPDATE_TIME == message.what) {
                Log.d(TAG, "updating time");

                // broadcast intent to update UI in MainActivity
                Intent intent = new Intent(UPDATE);
                MediaPlayerService.this.sendBroadcast(intent);

                // update the notification
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(NOTIFICATION_ID, createNotification());

                // trigger handler
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }

    /**
     * When gestures are on, use this handler to read the command from AccelerationService
     */
    class CheckSensorHandler extends Handler {

        private final static int UPDATE_RATE_MS = 500;

        @Override
        public void handleMessage(Message message) {
            if (MSG_CHECK_SENSOR == message.what) {

                int command = accelerationService.getCommand();
                if(command == VERTICAL) {
                    Log.d(TAG, "Got command from accelerationService: VERTICAL");
                    playMedia();
                }
                else if(command == HORIZONTAL) {
                    Log.d(TAG, "Got command from accelerationService: HORIZONTAL");
                    pauseMedia();
                }
                else if(command == IDLE) {
                    Log.d(TAG, "Got command from accelerationService: IDLE");
                }

                // trigger handler
                sendEmptyMessageDelayed(MSG_CHECK_SENSOR, UPDATE_RATE_MS);
            }
        }
    }

    /**
     * Formats the song duration integer into a readable format
     */
    public String createTimeLabel(int time) {
        String timeLabel = "";
        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        timeLabel = min + ":";
        if(sec < 10) timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }

    /**
     * Creates a notification for placing the service into the foreground
     *
     * @return a notification for interacting with the service when in the foreground
     */
    private Notification createNotification() {

        if(isMusicRestart) {
            // create notification
            builder = new NotificationCompat.Builder(this, channelID)
                    .setContentTitle(getString(R.string.defaultTitle))
                    .setContentText(getString(R.string.defaultDuration))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(channelID);
        }
        else {
            // create notification
            builder = new NotificationCompat.Builder(this, channelID)
                    .setContentTitle(getCurrentSong())
                    .setContentText(createTimeLabel(getCurrentPos()) + " / " + createTimeLabel(getDuration()))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setChannelId(channelID);
        }
        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(this, 0, resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        // define a notification action
        Intent startActionIntent = new Intent(this, MediaPlayerService.class);
        Intent pauseActionIntent = new Intent(this, MediaPlayerService.class);
        Intent stopActionIntent = new Intent(this, MediaPlayerService.class);
        Intent exitActionIntent = new Intent(this, MediaPlayerService.class);

        startActionIntent.setAction(ACTION_START);
        pauseActionIntent.setAction(ACTION_PAUSE);
        stopActionIntent.setAction(ACTION_STOP);
        exitActionIntent.setAction(ACTION_EXIT);

        PendingIntent startActionPendingIntent = PendingIntent.getService(this, 0,
                startActionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pauseActionPendingIntent = PendingIntent.getService(this, 0,
                pauseActionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stopActionPendingIntent = PendingIntent.getService(this, 0,
                stopActionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent exitActionPendingIntent = PendingIntent.getService(this, 0,
                exitActionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // create an action button in the notification
        builder.addAction(0, isMusicPlaying ? getString(R.string.pause) : getString(R.string.start),
                isMusicPlaying ? pauseActionPendingIntent : startActionPendingIntent);
        builder.addAction(0, getString(R.string.stop), stopActionPendingIntent);
        builder.addAction(0, getString(R.string.exit), exitActionPendingIntent);

        return builder.build();
    }

    private void createNotificationChannel() {

        if (Build.VERSION.SDK_INT < 26) {
            return;
        } else {

            NotificationChannel channel = new NotificationChannel(MediaPlayerService.channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(getString(R.string.channel_desc));
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);

            NotificationManager managerCompat = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            managerCompat.createNotificationChannel(channel);
        }
    }

    /**
     * Starts a notification in the foreground
     */
    public void foreground() {
        startForeground(NOTIFICATION_ID, createNotification());
    }

    /**
     * Moves the service to the background
     */
    public void background() {
        stopForeground(true);
    }

    /**
     * Local binder class to enable binding from the MainActivity.
     */
    public class RunServiceBinder extends Binder {
        MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }
}
