package si.uni_lj.fri.pbd.miniapp2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class AccelerationService extends Service implements SensorEventListener {

    private static final String TAG = AccelerationService.class.getSimpleName();
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private int command;
    private static final int IDLE = 0;
    private static final int VERTICAL = 1;
    private static final int HORIZONTAL = 2;

    float XtPrev;
    float YtPrev;
    float ZtPrev;
    float Xt;
    float Yt;
    float Zt;

    private final IBinder accelServiceBinder = new RunServiceBinder();


    @Override
    public void onCreate() {
        Log.d(TAG, "Creating service");
        super.onCreate();
        XtPrev = 0;
        YtPrev = 0;
        ZtPrev = 0;
        Xt = 0;
        Yt = 0;
        Zt = 0;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Starting service");
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        else
            Log.e(TAG, "There are no accelerometers on the device!");

        // SENSOR_DELAY_UI is equal to 60ms
        sensorManager.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_UI, new Handler());

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Binding service");
        return accelServiceBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float noise_threshold = 5.0f;

        XtPrev = Xt;
        YtPrev = Yt;
        ZtPrev = Zt;

        Xt = event.values[0];
        Yt = event.values[1];
        Zt = event.values[2];


        float dX = Math.abs(XtPrev-Xt);
        float dY = Math.abs(YtPrev-Yt);
        float dZ = Math.abs(ZtPrev-Zt);

        Log.d(TAG, "Sensor change! (dX: "+dX+", dY: "+dY+", dZ: "+dZ);

        if(dX <= noise_threshold)
            dX = 0;
        if(dY <= noise_threshold)
            dY = 0;
        if(dZ <= noise_threshold)
            dZ = 0;

        command = IDLE;

        if(dX > dZ) {
            Log.d(TAG, "Horizontal movement!");
            command = HORIZONTAL;
        }
        else if(dZ > dX) {
            Log.d(TAG, "Vertical movement!");
            command = VERTICAL;
        }

        if(command != IDLE) {
            // update MediaPlayerService
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    /**
     * Returns the command value
     */
    public int getCommand() {
        return command;
    }

    /**
     * Local binder class to enable binding from the MediaPlayerService.
     */
    public class RunServiceBinder extends Binder {
        AccelerationService getService() {
            return AccelerationService.this;
        }
    }
}
